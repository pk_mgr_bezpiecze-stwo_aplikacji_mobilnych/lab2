.class public final Landroidx/viewpager2/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/viewpager2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alpha:I = 0x7f03002c

.field public static final fastScrollEnabled:I = 0x7f0301a0

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f0301a1

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f0301a2

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f0301a3

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f0301a4

.field public static final font:I = 0x7f0301c8

.field public static final fontProviderAuthority:I = 0x7f0301ca

.field public static final fontProviderCerts:I = 0x7f0301cb

.field public static final fontProviderFetchStrategy:I = 0x7f0301cc

.field public static final fontProviderFetchTimeout:I = 0x7f0301cd

.field public static final fontProviderPackage:I = 0x7f0301ce

.field public static final fontProviderQuery:I = 0x7f0301cf

.field public static final fontStyle:I = 0x7f0301d1

.field public static final fontVariationSettings:I = 0x7f0301d2

.field public static final fontWeight:I = 0x7f0301d3

.field public static final layoutManager:I = 0x7f030236

.field public static final recyclerViewStyle:I = 0x7f030356

.field public static final reverseLayout:I = 0x7f03035c

.field public static final spanCount:I = 0x7f030390

.field public static final stackFromEnd:I = 0x7f03039b

.field public static final ttcIndex:I = 0x7f03046b


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
